The Snippets module allows you to store and display code snippets on Drupal sites.

## Installation

- Download and install required modules
- Go to Content -> "Add content" and click on "Code Snippet".

## Drush download

drush dl snippets geshifilter libraries views ctools
